<?php

namespace App\Repositories;

use App\Models\FarmPayablesDetail;

class FarmPayableDetailRepository
{

    private $farmPayableDetail;

    public function __construct(FarmPayablesDetail $farmPayableDetail)
    {
        $this->farmPayableDetail = $farmPayableDetail;
    }

    //This is saving a multiplication of
    //of quantity and amount
    //total amount in the farm_details but it should just save the items
    //amount and all its details....

    // public function savePayableItems($user, $farmPayableId,$itemId,$quantity,$amount,$totalAmount)
    // {
    //     $this->farmPayableDetail->farm_payables_id = $farmPayableId;
    //     $this->farmPayableDetail->item_id = $itemId;
    //     $this->farmPayableDetail->quantity = $quantity;
    //     $this->farmPayableDetail->price = $amount;
    //     $this->farmPayableDetail->total_amount = $totalAmount;
    //     $this->farmPayableDetail->created_by = $user->id;
    //     $this->farmPayableDetail->save();
    // }

    public function savePayableItems($farmPayableDetail)
    {
        FarmPayablesDetail::create($farmPayableDetail);
        // $farmPayableDetail = new FarmPayablesDetail();
        // $farmPayableDetail->farm_payables_id = $farmPayableId;
        // $farmPayableDetail->item_id = $itemId;
        // $farmPayableDetail->quantity = $quantity;
        // $farmPayableDetail->price = $amount;
        // $farmPayableDetail->total_amount = $amount;
        // $farmPayableDetail->created_by = $user->id;
        // $farmPayableDetail->save();
    }

    // public function savePayableItems($user, $farmPayableId,$itemId,$quantity,$amount,$totalAmount)
    // {
    //     $farmPayableDetail = new FarmPayablesDetail();
    //     $farmPayableDetail->farm_payables_id = $farmPayableId;
    //     $farmPayableDetail->item_id = $itemId;
    //     $farmPayableDetail->quantity = $quantity;
    //     $farmPayableDetail->price = $amount;
    //     $farmPayableDetail->total_amount = $amount;
    //     $farmPayableDetail->created_by = $user->id;
    //     $farmPayableDetail->save();
    // }
    public function updatePayableItems($id, $user, $farmPayableId, $itemId, $quantity, $amount, $totalAmount = null)
    {
        $farmPayableDetail = FarmPayablesDetail::firstOrNew(['id' => $id]);
        $farmPayableDetail->farm_payables_id = $farmPayableId;
        $farmPayableDetail->item_id = $itemId;
        $farmPayableDetail->quantity = $quantity;
        $farmPayableDetail->price = $amount;
        $farmPayableDetail->total_amount = $amount;
        $farmPayableDetail->created_by = $user->id;
        $farmPayableDetail->save();
    }

    public function updateFarmPayableDetailByfarmPayableId($farmPayableId, $farmPayableDetails)
    {
        FarmPayablesDetail::where('farm_payables_id', $farmPayableId)->first()->update($farmPayableDetails);
    }
    public function updateFarmPayableDetailById($id, $farmPayableDetails)
    {
        FarmPayablesDetail::where('farm_payables_id', $id)->update($farmPayableDetails);
    }

    public function getFarmBillsDetails($farmPayableId)
    {
        FarmPayablesDetail::where('farm_payables_id', $farmPayableId)->get();
    }

}
