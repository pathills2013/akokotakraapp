<?php

namespace App\Repositories;

use App\Models\FarmItem;
use App\Models\BillItem;

class FarmItemRepository
{

    private $farmItem;

    public function __construct(FarmItem $farmItem)
    {
        $this->farmItem = $farmItem;
    }

    public function createFarmItemFromStocking($user, $itemName, $amount,$bill)
    {
 
        return FarmItem::firstOrCreate(['item_name' => $itemName, 'farm_id' => $user->farm_id],
            ['created_by' => $user->id, 'price' => $amount,'status_bill_sale'=>$bill])->id;
        // $this->farmItem->item_name = $itemName;
        // $this->farmItem->price = $amount;
        // $this->farmItem->created_by = $user->id;
        // $this->farmItem->farm_id = $user->farm_id;
        // $this->farmItem->save();
        // return $this->farmItem->id;
    }

    public function getAllItems($farmId)
    {
         $query = ['farm_id'=> $farmId,'status_bill_sale'=>'Sale'];
        return FarmItem::where($query)->get();
    }

    public function getAllItemsF($farmId)
    {
      
        $query_item = ['farm_id'=>$farmId,'status_bill_sale'=>'Bill'];

        return FarmItem::where($query_item)->get();
    }


    public function getFarmDrugs($farmId)
    {
         $query = ['farm_id'=> $farmId,'item_category'=>'Drug'];
        return FarmItem::where($query)->get();
    }

    public function getFarmFeed($farmId)
    {
         $query = ['farm_id'=> $farmId,'item_category'=>'Feed'];
        return FarmItem::where($query)->get();
    }

    public function getAllBillItems($farmId)
    {
        return BillItem::where('farm_id', $farmId)->get();
    }

    public function createFarmItem($farmItem)
    {
        return FarmItem::create($farmItem);
    }

    public function updateFarmItem($id, $farmItem)
    {
        return FarmItem::where('id', $id)->update($farmItem);
    }

    public function deleteFarmItem($user,$id)
    {
        return FarmItem::where(['farm_id' => $user->farm_id, 'id' => $id])->delete();

        //return FarmItem::destroy($id);
    }

    public function findFarmItemById($id)
    {
        return FarmItem::find($id);
    }


    public function findFarmItemAndUpdate($id,$item_name,$item_amount)
    {
        $FarmItem = FarmItem::findOrFail($id);
        $FarmItem->item_name = $item_name;
        $FarmItem->price = $item_amount;
        
        $FarmItem->save(); 
    }
}
