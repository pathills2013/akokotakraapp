<?php

namespace App\Repositories;

use App\Models\Journal;
 
class JournalsRepository
{

    private $journal;

    public function __construct(Journal $journal)
    {
        $this->journal = $journal;
    }

    public function createJournal($journal)
    {
        return Journal::create($journal);
    }


    public function saveJournal($farm_id,$acc_name,$acc_type,$description,$debit,$credit,$bal,$created_by){
        $this->journal->farm_id = $farm_id;
        $this->journal->acc_name = $acc_name;
        $this->journal->acc_type = $acc_type;
        $this->journal->description = $description;
        $this->journal->debit = $debit;
        $this->journal->credit = $credit;
        $this->journal->bal = $bal;
        $this->journal->created_by = $created_by;
        $this->journal->save();
        return $this->journal->id;
    }

    public function getAllJournal($farmId)
    {
        return Journal::where('farm_id', $farmId)->get();
    }

    // public function getAllItemsF($farmId)
    // {
    //    // $query = ['farm_id'=> $farmId];

    //     return FarmItem::where('farm_id',$farmId)->orWhereNull('status_bill_sale')->get();
    // }

  

    // public function updateBillItem($id, $billItem)
    // {
    //     return BillItem::where('id', $id)->update($billItem);
    // }

    

    // public function deleteBillItem2($user,$id)
    // {
    //     return BillItem::where(['farm_id' => $user->farm_id, 'id' => $id])->delete();

       
    // }
    
     
}
