<?php

namespace App\Repositories;

use App\Models\FarmPayroll;

class PayrollRepository
{

    public function getAllFarmPayroll($farm_id)
    {
        return FarmPayroll::where('farm_id', $farm_id)->get();
    }

    public function createFarmPayroll($farmPayroll)
    {
        return FarmPayroll::create($farmPayroll);
    }

    public function updateFarmPayroll($id, $farmId, $farmPayroll)
    {
        return FarmPayroll::where(['id'=> $id, 'farm_id' => $farmId])->update($farmPayroll);
    }

    public function getFarmPayrollById($id)
    {
        return FarmPayroll::find($id);
    }

    public function deleteFarmPayroll($id, $farmId)
    {
        return FarmPayroll::where(['id' => $id, 'farm_id' => $farmId])->delete();
    }
}
