<?php

namespace App\Repositories;

use App\Models\FarmAccount;

class FarmAccountRepository
{

    protected $farm_account;

    public function __construct(FarmAccount $farm_account)
    {
        $this->farm_account = $farm_account;
    }

    public function createOrUpdateFarmAccount($user, $farm_name, $country, $currency, $farm_capacity, $farm_address)
    {
        // $user  = Auth::user();
        // FarmAccount::where('id', $user->farm_account)
        //   ->update(['farm_name' => $farm_name,''=>$country_code]);
        if ($user->farm_account) {
            $this->farm_account = $user->farm_account;
        }
        $this->farm_account->farm_name = $farm_name;
        $this->farm_account->country = $country;
        $this->farm_account->currency = $currency;
        $this->farm_account->farm_code = $currency;
        $this->farm_account->farm_capacity = $farm_capacity;
        $this->farm_account->farm_address = $farm_address;
        $this->farm_account->created_by = $user->id;
        $this->farm_account->save();
        $user->farm_id = $this->farm_account->id;
        $user->save();
    }

}
