<?php

namespace App\Repositories;

use App\Models\ChartsOfAccount;
 
class ChartsOfAccountRepository
{

    private $chartsofaccountsItem;

    public function __construct(ChartsOfAccount $chartsofaccountsItem)
    {
        $this->ChartsOfAccount = $chartsofaccountsItem;
    }

    public function createChartsOfAccounts($chartsofaccountsItem)
    {
        return ChartsOfAccount::create($chartsofaccountsItem);
    }

    public function getAllAccounts($farmId)
    {
        return ChartsOfAccount::where('farm_id', $farmId)->get();
    }

    public function getAccountTypeByname($farmId,$accname){
           $query = ['farm_id'=> $farmId,'acc_name'=> $accname];

         $acctype = ChartsOfAccount::where($query)->first(['acc_type']);
         return $acctype->acc_type;

    }

    // public function getAllItemsF($farmId)
    // {
    //    // $query = ['farm_id'=> $farmId];

    //     return FarmItem::where('farm_id',$farmId)->orWhereNull('status_bill_sale')->get();
    // }

  

    // public function updateBillItem($id, $billItem)
    // {
    //     return BillItem::where('id', $id)->update($billItem);
    // }

    

    // public function deleteBillItem2($user,$id)
    // {
    //     return BillItem::where(['farm_id' => $user->farm_id, 'id' => $id])->delete();

       
    // }
    
     
}
