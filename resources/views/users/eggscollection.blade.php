@extends('users/layouts.eggs_collection_master')
@section('eggs')

<article class="content forms-page">
            <div class="title-block">
            	<div class="title">
                        {{-- <a class="btn btn-primary" href="{{url('account/collectlist')}}"> Collect Egg </a> --}}
				    <button type="button" class="btn btn-primary" id="btnadd" data-toggle="modal" data-target="#addEggs">Collect Eggs</button>
                    
				</div>  
                <h3 class="title">Eggs Collection Summary</h3>

                <p class="title-description"> 
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p> 
            </div>

       
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Total Eggs Collected</th>
                                        <th>Good Eggs Remaining</th>
                                        <th>Broken Eggs Remaining</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if(!$getSumOfEggs) 
                                         <td> No Eggs Recorded yet </td>
                                        @else 
                                        <td>{{$getSumOfEggs}}</td>
                                        @endif  
                                        @if(!$goodeggs) 
                                        <td> No Broken Eggs Recorded yet </td>
                                        @else   
                                        <td>{{$goodeggs}}</td>
                                        @endif  
                                        @if(!$brokeneggs) 
                                        <td> No Broken Eggs Recorded yet </td>
                                        @else   
                                        <td>{{$brokeneggs}}</td>
                                        @endif   
                                    </tr>
                               <tbody>
                            </table>
                        </div>    
                    </div>
                </div>
            </div>




            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="col-md-12">
                            <div class="row form-group">  
                               <div class="col-4">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Status</option>
                                            <option>All</option>
                                            <option>Good</option>
                                            <option>Broken</option>
                                            
                                        </select>
                                    </div>  
                                </div>
                                <div class="col-4">
                                    <input type="text" class="form-control" placeholder="From"> 
                                </div>
                                <div class="col-4">
                                    <input type="text" class="form-control" placeholder="To"> 
                                </div>
                            </div>
                        </div>


                        <div class="container mt-3">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a style="text-decoration: none;" class="nav-link active" data-toggle="tab" href="#good">Good</a>
                                </li>
                                <li class="nav-item">
                                    <a style="text-decoration: none;" class="nav-link" data-toggle="tab" href="#broken">Broken</a>
                                </li>
                                <li class="nav-item">
                                    <a style="text-decoration: none;" class="nav-link" data-toggle="tab" href="#all">All</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="good" class="container tab-pane active"><br>
                                    <div class="table-responsive">
                                        <table class="" rules="" style="width: 100%;" >
                                            <thead>
                                                <tr>    
                                                    <th>Date</th>
                                                    <th>Number of trays</th>
                                                    <th>Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!$getAllEggsForFarmgood) 
                                                <tr>
                                                    <td> No Records </td>
                                                </tr>
                                                @else   
                                                @foreach ($getAllEggsForFarmgood as $pen)  
                                                <tr>
                                                    <td>{{\Carbon\Carbon::parse($pen->date_recorded)->format('jS \o\f F, Y')}}</td>        
                                                    <td> {{$pen->tray_quantity}} </td>  
                                                    <td>{{$pen->type_of_egg}} </td> 
                                                </tr> 
                                                @endforeach   
                                                @endif   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div id="broken" class="container tab-pane fade"><br>
                                    <div class="table-responsive">
                                        <table class="" rules="" style="width: 100%;" >
                                            <thead>
                                                <tr>   
                                                    <th>Date</th>
                                                    <th>Number of trays</th>
                                                    <th> Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!$brokeneggs) 
                                                <tr>
                                                    <td> No Records </td>
                                                </tr>
                                                @else   
                                                @foreach ($brokeneggs as $pen)  
                                                <tr>
                                                    <td>{{\Carbon\Carbon::parse($pen->date_recorded)->format('jS \o\f F, Y')}}</td>     
                                                    <td> {{$pen->tray_quantity}} </td>  
                                                    <td>{{$pen->type_of_egg}} </td>        
                                                </tr> 
                                                @endforeach   
                                                @endif   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                <div id="all" class="container tab-pane fade"><br>
                                    <div class="table-responsive">
                                        <table class="" rules="" style="width: 100%;" >
                                            <thead>
                                                <tr>   
                                                    <th>Date</th>
                                                    <th>Number of trays</th>
                                                    <th> Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!$getAllEggsForFarmgood) 
                                                <tr>
                                                    <td> No Records </td>
                                                </tr>
                                                @else   
                                                @foreach ($getAllEggsForFarmgood as $pen)  
                                                <tr>
                                                    <td>{{\Carbon\Carbon::parse($pen->date_recorded)->format('jS \o\f F, Y')}}</td>       
                                                    <td> {{$pen->tray_quantity}} </td>  
                                                    <td>{{$pen->type_of_egg}} </td> 
                                                </tr> 
                                                @endforeach   
                                                @endif   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
 
</article>



 <!-- ADD P MODAL -->
    <div class="modal fade" id="addEggs" tabindex="-1" role="dialog" aria-labelledby="addEggs" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <section class="section">
                <div class="col-md-12">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelTitleId">Eggs Recording</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <div class="card card-block sameheight-item">
                                <form method="POST" action="{{route('account.collectEggs')}}">
                                        {{csrf_field()}}

                                <div class="row form-group">
                                    <div class="col-5">
                                        <div class="form-group row">
                                            <label class="col-sm-4 form-control-label">Batch  </label>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <select name="batch" class="form-control" required>
                                                        <option>Select Batch Id</option>
                                                        <option>1 </option>
                                                        <option>2</option>
                                                       
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-7">
                                        <div class="form-group row">
                                            <label class="col-sm-5 form-control-label">Penhouse</label>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <select  name="penhouse" class="form-control" required>
                                                        <option>Select Penhouse</option>
                                                        <option> 1 </option>
                                                        <option> 2</option>
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>      
                                    
                                </div>

                                <div class="row form-group">
                                    <div class="col-5">
                                        <div class="form-group row">
                                            <label class="col-sm-4 form-control-label">Type</label>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <select name="eggtype" class="form-control" required>
                                                        <option>Select Egg Type</option>
                                                        <option>Good</option>
                                                        <option>Broken</option>
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>  
                                    
                                    <div class="col-7">
                                        <div class="form-group row">
                                            <label class="col-sm-5 form-control-label">Quantity Of Trays</label>
                                            <div class="col-sm-7">
                                                <input  name="qty" required type="number" class="form-control" id="" placeholder="Enter number of trays"> 
                                            </div>
                                        </div>
                                    </div>   
                                </div>

                                <div class="row form-group">
                                    <div class="col-5">
                                        <div class="form-group row">
                                            <label class="col-sm-4 form-control-label">Date</label>
                                            <div class="col-sm-8">
                                                <input name="datecollected" required type="text" class="form-control" id="" placeholder="dd/mm/yy"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="form-group row">
                                            <label style="color:red" class="col-sm-5 form-control-label">Eggs Remaining</label>
                                            <div class="col-sm-7">
                                                <input required type="number" class="form-control" id="" placeholder="Enter eggs remaining"> 
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>   
                            </form>
                        </div>
                    </div>
                </div>
            </section>    
        </div>
    </div>
@endsection