@extends('users/layouts.onboardingcomplete-master')
@section('complete')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    <ul class="progressbar">
                        <li class="active1">Create Account</li>
                        <li class="active2">Setup Farm</li>
                        <li class="active3">Setup Penhouse</li>
                        <li class="active4">Setup Stock</li>
                        <li class="active5">Completed</li>
                    </ul>
                </div>


                <div id="smartwizard" class="pt-3">
                	<div>
                        <div class="wz-content-wrapper mt-5 mb-5" >
       
                                <div class="text-center"> 
                                        <h1>Congratulations</h1>
                                        <p>You have succesfully setup your poultry farm.</p>
                                        <img style="" alt="Loading" src="{{ URL::to('akokotakra-01.png') }}"/>
                                        <p>
                                            Akokotakra farm management software enables </br> the poultry farmer to record,
                                             monitor, keep track,</br>  and analyze all their farm operations easily. </br>
                                             Feed, Drugs, Birds, Eggs collection and other activities like</br> Sales to customers,</br>
                                             Bills from vendors etc are all in one place and effectively managed with few clicks.
        
        
                                        </p>
                                    </div>
                                       
                                
                                    <div class="text-center">
                                     <button type="button" id="finbtn" class="btn btn-primary btn-sm mt-3" data-toggle="modal" data-target="#addStock">Continue</button>
                                    </div>
        
         
                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        
        // document.getElementById("finbtn").onclick = function () {
        //     location.href = "{{url('onboarding/stocking')}}";
        // };


document.getElementById("finbtn").onclick = function () {
            location.href = "{{url('account/dashboard')}}";
        };

        document.getElementById("prevbtn").onclick = function () {
            location.href = "{{url('onboarding/penhouse')}}";
        };
    </script>
@endsection