@extends('users/layouts.onboard-penhouse-master')
@section('penhouse')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                        @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif
                    <ul class="progressbar">
                        <li class="active1">Create Account</li>
                        <li class="active2">Setup Farm</li>
                        <li class="active3">Setup Penhouse</li>
                        <li class="active4">Setup Stock</li> 
                        <li class="active5">Completed</li>
                    </ul>
                </div>
                <div id="smartwizard" class="pt-3">
                	<div> 
			            <div class="wz-content-wrapper mt-5 mb-5">
			                <div class="text-center">
			                    <h1>Welcome let's setup your penhouse's</h1>
			                    <p>You're well on your way to digitising your poultry farm management and becoming a superhero. First, let's setup your farm.</p>

			                    <img class="mt-2 d-block mx-auto" src="{{ URL::to('vendor/poultry-farm.jpg') }}" alt="Poultry Farm" height="100" width="auto">

			                    <button type="button" class="btn btn-primary btn-sm mt-3" data-toggle="modal" data-target="#addBranch">Add Penhouse</button>
                            </div>
                                <div class="table-responsive">
                                     <table id="onboardingDataTable" class="table table-striped table-bordered table-hover" style="">
                                        <thead>
                                            <tr>
                                                <th>Pen house name</th>
                                                <th>Pen number</th>
                                                <th>Is pen house stocked?</th>
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                           @foreach ($penHouse as $pen)
                                            <tr>
                                                <td>{{$pen->pen_name}}</td>
                                                <td> {{$pen->pen_number}}</td>
                                                <td>{{$pen->stocked}}</td>
                                                <td> 
                                                    <button class="edit-modal  btn btn-info" data-number="{{$pen->pen_number}}" data-house="{{$pen->pen_name}}" data-id="{{$pen->id}}">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    
                                                    <button class="delete-modal btn btn-danger" data-number="{{$pen->pen_number}}" data-house="{{$pen->pen_name}}" data-id="{{$pen->id}}">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                     </table>
                                </div>                
			                 <div class="modal-footer" style="margin-top: 30px;">
                    			<button id="prevbtn" type="button" class="btn btn-primary">Previous</button>
                    			<button id="nxtbtn" {{$penHouse ? '':'disabled'}} type="button" class="btn btn-primary">Next</button>
                			</div>
			            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- ADD PENHOUSE MODAL -->
    <div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId">Add New Penhouse</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="new_branch_form" method="POST" action="{{route('onboarding.penhouse')}}" class="mt-5">
                <div class="modal-body">
                        {{csrf_field()}} 
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label>Pen House Name</label>
                                    <input type="text" class="form-control" name="penHouse"  placeholder="Penhouse Name" required>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label>Pen Number</label>
                                    <input type="number" value="{{$penNum+1}}" class="form-control" name="penNumber"  placeholder="Penhouse Number" required>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="return confirm('Are you sure you want to proceed?');" class="btn btn-primary">Save</button>
                </div>
                 </form>
            </div>
        </div>
    </div>




    <!-- EDIT PENHOUSE MODAL -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId">Add New Penhouse</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="new_branch_form" method="POST" action="{{route('onboarding.penhouseupdate')}}">
         
                <div class="modal-body">
                        {{csrf_field()}}
                        
                        <div class="row">
                                <input type="hidden" value="upt" readonly class="form-control" name="updatecheck" aria-describedby="branchContact" placeholder="Penhouse ID" required>
                                           
                                <div  style="display: none"  class="col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>ID</label>
                                            <input type="text" readonly class="form-control" name="penhouseid" id="penhouseid" aria-describedby="branchContact" placeholder="Penhouse ID" required>
                                        </div>
                                </div>
                           
                         <div class="col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label>Pen House Name</label>
                                    <input type="text" class="form-control" name="penHouse" id="penhouse" aria-describedby="branchContact" placeholder="Penhouse Name" required>
                                </div>
                            </div>  

                        <div class="col-md-12 col-lg-6">
                                <div class="form-group">
                                        <label>Pen Number</label>
                                <input type="text" value="" class="form-control" name="penNumber" id="pennumber" aria-describedby="locationAutocomplete" placeholder="Penhouse Number" required>
                                </div>
                            </div>  
                        </div>
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
                 </form>
            </div>
        </div>
    </div>


      <!-- DELETE PENHOUSE MODAL -->
      <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelTitleId">Add New Penhouse</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="new_branch_form" method="POST" action="{{route('onboarding.penhousedelete')}}" class="mt-5">
             
                    <div class="modal-body">
                            {{csrf_field()}}
                            
                            <div class="row">
    
                                    <div class="col-md-12 col-lg-6">
                                            <div class="form-group">
                                                {{-- <label>ID</label> --}}
                                                <input type="hidden" readonly class="form-control" name="penhouseid" id="penhouseid1" aria-describedby="branchContact" placeholder="Penhouse ID" required>
                                               
                                            </div>
                                        </div>
                               
                             <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                        {{-- <label>Pen House Name</label> --}}
                                        <input type="hidden" class="form-control" name="penHouse" id="penhouse1" aria-describedby="branchContact" placeholder="Penhouse Name" required>
                                    </div>
                                </div>  
    
                            <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                            {{-- <label>Pen Number</label> --}}
                                    <input type="hidden" value="" class="form-control" name="penNumber" id="pennumber1" aria-describedby="locationAutocomplete" placeholder="Penhouse Number" required>
                                    </div>
                                </div>  
                            </div>
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button class="btn btn-danger">Yes</button>
                    </div>
                     </form>
                </div>
            </div>
        </div>

<script type="text/javascript">

   // Edit a penhouse
   $(document).on('click', '.edit-modal', function() {
            $('.modal-title').text('Edit');
            $('#penhouseid').val($(this).data('id'));
            $('#penhouse').val($(this).data('house'));
            $('#pennumber').val($(this).data('number'));
           // id = $('#id_edit').val();
            $('#editModal').modal('show');
        });

     // Delete a penhouse
         $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Are you sure you want to delete record?');
            $('#penhouseid1').val($(this).data('id'));
            $('#penhouse1').val($(this).data('house'));
            $('#pennumber1').val($(this).data('number'));
           // id = $('#id_edit').val();
            $('#deleteModal').modal('show');
        });



    	  
    document.getElementById("prevbtn").onclick = function () {
        location.href = "{{url('onboarding/farmsetup')}}";
    };
    document.getElementById("nxtbtn").onclick = function () {
        location.href = "{{url('onboarding/stocking')}}";
    };
</script>

@endsection