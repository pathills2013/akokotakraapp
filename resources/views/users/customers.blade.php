@extends('users/layouts.customermaster') 
@section('customer')
<article class="content forms-page">
    <div class="title-block">
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="title">
            <button type="button" class="reset btn btn-primary" data-href="{{route('account.customercreate')}}" id="btnadd" data-toggle="modal"
                data-target="#addCustomer">Add Customer</button>

        </div>
        <h3 class="title"> Customer</h3>

        <p class="title-description">
     
        </p>
    </div>

    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <section class="example">
                            <div class="table-responsive">
                                <table id="customerTable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Date Created</th>
                                            <th>Name </th>
                                            <th>Email</th>
                                            <th>Location</th>
                                            {{-- <th>Action</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($customers) 
                                        @foreach($customers as $customer)
                                        <tr id="{{$customer->id}}">
                                            <td>{{\Carbon\Carbon::parse($customer->created_at)->format('jS \o\f F, Y')}}</td>
                                            <td>{{$customer->name}} <br>{{$customer->contact}}</td>
                                            <td>{{$customer->email}}</td>
                                            <td>{{$customer->residence}}</td>
{{-- 
                                            <td><button data-toggle="modal" data-target="#addCustomer" class="update-customer edit-modal  btn btn-info"
                                                    data-customer="{{$customer}}" data-href="{{route('account.customerupdate',['id'=>$customer->id])}}"> <i class="fa fa-pencil"></i></button>
                                                <button href="" class="delete-customer-modal delete-modal btn btn-danger" data-token="{{csrf_token()}}" data-toggle="modal" data-target="#deleteCustomerModal"
                                                    data-href="{{route('account.customerdelete',['id'=>$customer->id])}}"><i class="fa fa-trash"></i></button>
                                            </td>

                                             --}}
                                        </tr>
                                        @endforeach 
                                        @endisset 
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>

</article>


<!-- Modal -->
<div class="modal fade" id="deleteCustomerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete customer
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-dismiss="modal" data-token="{{csrf_token()}}" class="delete-customer btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- ADD P MODAL -->
<div class="modal fade" id="addCustomer" tabindex="-1" role="dialog" aria-labelledby="addCustomer" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <section class="section">
            <div class="col-md-12">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId">Customer</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-content">
                    <div class="card card-block sameheight-item">
                        <form method="POST" id="customerForm" action="{{route('account.customercreate')}}">
                            {{csrf_field()}}
                            <div class="row form-group">
                                <div class="col-7">
                                    <div class="form-group row">
                                        <label class="col-sm-5 form-control-label">Name of Customer <label style="color: red;">*</label></label>
                                        <div class="col-sm-7">
                                            <input required type="text" name="name" class="form-control" id="name" placeholder="Customer's name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group row">
                                        <label {{-- for="inputEmail3" --}} class="col-sm-4 form-control-label">Contact<label style="color: red;">*</label></label>
                                        <div class="col-sm-8">
                                            <input type="number" name="contact" class="form-control" id="contact" placeholder="024XXXXXXXX">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-7">
                                    <div class="form-group row">
                                        <label class="col-sm-5 form-control-label">Location<label style="color: red;">*</label></label>
                                        <div class="col-sm-7">
                                            <input required name="residence" type="text" class="form-control" id="residence" placeholder="Enter customer's location">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-5">
                                    <div class="form-group row">
                                        <label class="col-sm-4 form-control-label">Email</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="email" class="form-control" id="email" placeholder="client@example.com">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-7">
                                    <div class="form-group row">
                                        <label class="col-sm-5 form-control-label">Country</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="country" placeholder="Enter customer's country">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection