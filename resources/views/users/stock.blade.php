@extends('users/layouts.stockmaster') 
@section('stock')


<article class="content forms-page">
    <div class="title-block">
        <div class="title">
               @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif   
            <button type="button" class="btn btn-primary" id="btnadd" data-toggle="modal" data-target="#addStock">Add Stock</button>
        </div>
        <!-- stocking header-->
        <h3 class="title">Stock</h3>
        
        <p class="title-description">

           
        </p>


         
    </div>

    <!--stocking data-table view -->
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <section class="example">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Date Stocked</th>
                                            <th>Batch</th>
                                            <th>Penhouse</th>
                                            <th>Bird Type</th>
                                            <th>Quantity</th>
                                            <th>Unit Cost </th>
                                            <th>Total </th>
                                          
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @if(!$penHouseStockingdas)
                                        <tr>
                                            <td> No Records </td>
                                        </tr>
                                        @else @foreach($penHouseStockingdas as $pen)
                                        <tr id="{{$pen->id}}" >
                                            <td>{{\Carbon\Carbon::parse($pen->date_paid)->format('jS \o\f F, Y')}}</td>
                                            <td>{{$pen->stock_tracking->batch_id}}</td>
                                            <td>{{$pen->pen_house->pen_number}}</td>
                                            <td>{{$pen->type_of_bird}}</td>


                                            <td>{{$pen->number_of_stock}}</td>
                                            <td>{{$pen->farm_payable->farm_payables_details[0]->price}} </td>

                                         
                                                 <td>{{$pen->farm_payable->farm_payables_details[0]->total_amount}}</td>
                                       

                                            <td>
                                                {{-- <button class="btn btn-success">
                                                <i class="fa fa-eye"></i>
                                            </button> --}}
                                                <button class="btn btn-info edit-penstocking" data-toggle="modal" data-target="#editStock" data-typeofbird="{{$pen->type_of_bird}}"
                                                    data-numberofstock="{{$pen->number_of_stock}}" data-batch="{{$pen->stock_tracking->batch_id}}"
                                                    data-penhouse="{{$pen->pen_house_id}}" data-dateissued="{{$pen->date_issued}}"
                                                    data-href="{{route('birds.updatestocking',['id'=>$pen->id])}}" data-token="{{csrf_token()}}"
                                                    data-vendor="{{$pen->vendor_id}}"
                                                    data-farmitemid="{{$pen->farm_payable->farm_payables_details[0]->item_id}}" 
                                                    data-amount="{{$pen->farm_payable->farm_payables_details[0]->price}}" 
                                                    data-dtstocked="{{$pen->date_stocked}}" 
                                                    {{-- data-amount="{{$pen->farm_payable->farm_payables_details[0]->price}}" --}}

                                                    data-datedue="{{$pen->date_due}}">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            {{-- <button class="delete-stocking-url delete-modal btn btn-danger" data-toggle="modal" data-target="#deletePenStockingModal" data-href="{{route('account.deleteStockingOnDashboard',['id'=>$pen->id])}}">
                                                <i class="fa fa-trash"></i>
                                            </button><br> --}}

                                            </td>
                                        </tr>
                                        @endforeach @endif
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>

</article>





    <!--delete  Modal -->
    <div class="modal fade" id="deletePenStockingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Penhose Stocking Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete Penhouse stocking
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-dismiss="modal" data-token="{{csrf_token()}}" class="delete-stocking-modal btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>


<!-- ADD NEW STOCK MODAL -->
<div class="modal fade" id="addStock" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <section class="section">
            <div class="modal-header">
                <h4 class="modal-title" id="modelTitleId">Stock Recording</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-content card-block">
                <form method="POST" action="{{route('birds.stocking')}}" class="mt-5">
                    {{csrf_field()}}
                    <div class="row form-group">
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Bird Type </label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="typeOfBird" required>
                                        <option></option>
                                        <option value="Layers(DOC)">Layers(DOC)</option>
                                        <option value="Broilers(DOC)">Broilers(DOC)</option>
                                        <option value="Cockerel(DOC)">Cockerel(DOC)</option>
                                        <option value="Guinea fowls(DOC)">Guinea fowls(DOC)</option>
                                        <option value="Turkey(DOC)">Turkey(DOC)</option>
                                        <option value="Ducks(DOC)">Ducks(DOC)</option>
                                        </select>
                                </div>
                            </div>
                        </div>


                        <input required type="hidden" value="YES" class="form-control" name="active" placeholder="Enter number of stock ">
                        <input required type="hidden" value="Bill" class="form-control" name="status_bill_sale" placeholder="Enter number of stock ">
                        <input required type="hidden" value="Product" class="form-control" name="expense_category" placeholder="Enter number of stock ">
                       
                        <div class="col-7">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Quantity of stock</label>
                                <div class="col-sm-7">
                                    <input required type="number" class="form-control" name="numberOfStock" placeholder="Enter number of stock ">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Penhouse</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="penHouse" required>
                                            <option></option>
                                            @foreach ($penHouses as $pen)
                                            <option value="{{$pen->id}}">{{$pen->pen_name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Unit Price</label>
                                <div class="col-sm-7">
                                    <input required type="number" class="form-control" name="amount" placeholder="Enter price of stock">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Batch</label>
                                <div class="col-sm-8">
                                    <input required type="number" class="form-control" value="{{$maxBatch+1}}" name="batchId" placeholder="Batch Number">
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Vendor</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="vendor" required>
                                            <option>Vendor</option>
                                            @foreach ($vendors as $vendor)
                                            <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Date</label>
                                <div class="col-sm-8">
                                    <input required autocomplete="off" type="text" class="form-control example1" name="dateIssued" placeholder="dd/mm/yy">
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Description</label>
                                <div class="col-sm-7">
                                    <textarea rows="2" name="description" class="form-control" placeholder="Description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-5">
                            <label>Has the stock been paid for?</label><br>
                            <label>Default mode of payment would be CASH if yes</label><br>
                            <input type="radio" name="paid" value="YES" onclick="check(this.value);"> Yes<br>
                            <input type="radio" name="paid" value="NO" onclick="check(this.value)"> No
                        </div>
                        <div class="col-7" id="dueDate" style="Display:none;">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Date Due</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="dateDue" name="dateDue" placeholder="dd/mm/yy">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>

<!-- EDIT STOCK MODAL -->
<div class="modal fade" id="editStock" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <section class="section">
            <div class="modal-header">
                <h4 class="modal-title" id="modelTitleId">Edit Stock Recording</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-content card-block">
                <form id="editpenstockginForm" method="POST">

                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="row form-group">
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Bird Type </label>
                                <div class="col-sm-8">
                                    <select id="typeOfBird" class="form-control" name="typeOfBird" required>
                                            <option></option>
                                            <option value="Broiler(DOC)">Broiler(DOC)</option>
                                            <option value="Layer(DOC)">Layer(DOC)</option>
                                            <option value="Cockerel(DOC)">Cockerel(DOC)</option>
                                            <option value="Guinea Fowl(DOC)">Guinea Fowl(DOC)</option>
                                            <option value="Turkey(DOC)">Turkey(DOC)</option>
                                            <option value="Ducks(DOC)">Ducks(DOC)</option>
                                            <option value="Quails(DOC)">Quails(DOC)</option>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Quantity</label>
                                <div class="col-sm-7">
                                    <input id="numberOfStock" required type="number" class="form-control" name="numberOfStock" placeholder="Enter number of stock ">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">

{{-- 
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Penhouse</label>
                                <div class="col-sm-8">
                                    <label>

                                    {{-- {{$pen->pen_house->pen_name}}   
                                    
                                    </label>
                                    <select id="penHouse" class="form-control" name="penHouse">
                                            <option value="">Penhouse</option>
                                            @foreach ($penHouses as $pen)
                                            <option value="{{$pen->id}}">{{$pen->pen_name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div> --}}


                        
                        <input id="farmitemid" required type="hidden" class="form-control" name="farmitemid" placeholder="Enter price of stock">
                                


                         
                       
                    </div>

                    <div class="row form-group">
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Batch</label>
                                <div class="col-sm-8">
                                    <input id="batch" required type="number" class="form-control" value="{{$maxBatch+1}}" name="batchId" placeholder="Batch Number">
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Vendor</label>
                                <div class="col-sm-7">
                                    <select id="vendor" class="form-control" name="vendor" required>
                                            <option>Vendor</option>
                                            @foreach ($vendors as $vendor)
                                            <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-5">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Date</label>
                                <div class="col-sm-8">
                                    <input id="dtstocked" autocomplete="off" required type="text" class="form-control example1" name="dtstocked" placeholder="dd/mm/yy">
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <label class="col-sm-4 form-control-label">Description</label>
                                <div class="col-sm-7">
                                    <textarea id="description" rows="2" name="description" class="form-control" placeholder="Description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="row form-group">
                            <div class="col-5">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Unit Price</label>
                                    <div class="col-sm-8">
                                            <input id="amount" required type="number" class="form-control" name="amount" placeholder="Enter price of stock">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label"></label>
                                    <div class="col-sm-7">
                                         
                                    </div>
                                </div>
                            </div>
                        </div>


   

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>

<!-- ADD TO EXISTING BATCH MODAL -->
<div class="modal fade" id="addToExistingStock" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <section class="section">
            <div class="col-md-12">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId">Stock Recording</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-content card-block">
                    <form method="POST" action="{{route('birds.stocking')}}" class="mt-5">
                        {{csrf_field()}}
                        <div class="row form-group">
                            <div class="col-5">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Bird Type </label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="typeOfBird" required>
                                               
                                            </select>
                                    </div>
                                </div>
                            </div>
                                       

                            <div class="col-7">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Quantity of stock</label>
                                    <div class="col-sm-7">
                                        <input required type="number" class="form-control" name="numberOfStock" placeholder="Enter number of stock ">
                                   
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col-5">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Penhouse</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="penHouse" required>
                                                <option>Penhouse</option>
                                                @foreach ($penHouses as $pen)
                                                <option value="{{$pen->id}}">{{$pen->pen_name}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Unit Price</label>
                                    <div class="col-sm-7">
                                        <input required type="number" class="form-control" name="amount" placeholder="Enter price of stock">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-5">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Batch</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="batchId" required>
                                                <option>Batch</option>
                                                @foreach ($batches as $batch)
                                                <option value="{{$batch->id}}">{{$batch->batch_id}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Vendor</label>
                                    <div class="col-sm-7">
                                        <select class="form-control" name="vendor" required>
                                                <option>Vendor</option>
                                                @foreach ($vendors as $vendor)
                                                <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-5">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Date</label>
                                    <div class="col-sm-8">
                                        <input required type="text" class="form-control example1" name="dateIssued" placeholder="dd/mm/yy">
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Description</label>
                                    <div class="col-sm-7">
                                        <textarea rows="2" name="description" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-5">
                                <label>Has the stock been paid for?</label><br>
                                <input type="radio" name="paid" value="YES" onclick="checking(this.value);"> Yes<br>
                                <input type="radio" name="paid" value="NO" onclick="checking(this.value)"> No
                            </div>
                            <div class="col-7" id="dateDue" style="Display:none;">
                                <div class="row">
                                    <label class="col-sm-4 form-control-label">Date Due</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="dateDue" name="dateDue" placeholder="dd/mm/yy">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>

 <script src="{{url('/')}}/assestdash/js/custom.js"></script>

 



<link rel="stylesheet" href="{{ URL::to('vendor/jquery/bootstrap-datepicker.min.css')}}">
{{-- <script src="{{ URL::to('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ URL::to('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ URL::to('vendor/jquery/bootstrap.min.js')}}"></script>  --}}
<script src="{{ URL::to('vendor/jquery/bootstrap-datepicker.min.js')}}"></script> 


<script type="text/javascript">
    function check(value){
            if (value === 'NO'){
                document.getElementById('dueDate').style.display = 'block';    
            }
            else{
                document.getElementById('dueDate').style.display = 'none';   
            }
        }

        function checking(value){
            if (value === 'NO'){
                document.getElementById('dateDue').style.display = 'block';    
            }
            else{
                document.getElementById('dateDue').style.display = 'none';   
            }
        }




//set url to delete stocking

$(".delete-stocking-url").click(function () {
    var url = $(this).data("href");
    var newurl = $('.delete-stocking-modal').data('href', url); //setter
    // var a = $('.delete-item').data('href'); //getter

});


//delete stocking
$(".delete-stocking-modal").click(function () { 
        var url = $(this).data("href");
         console.log("url " + url);
        var token = $(this).data("token");
        $.ajax(
            {
                url: url,
                type: 'DELETE',
                data: {
                    "_token": token,
                },
                success: function (data) { 
                    // var json = JSON.parse(data);
                    if (data['status'] == "1") {
                    $('table#dataTable tr#' + data['id']).remove();

                      toastr.success('Pen Stocking record deleted successfully');
                    // location.reload();
                    // $(window).on('load', function () {
                    //     $('#deleteModal').modal('show');
                    // });
                    // console.log("it Work");
                     console.log(data); 
                    }else{
                        toastr.error('An error occured!')
                    }

                },
                error: function (data, textStatus, errorThrown) {
                     console.log("error");
                    // console.log(textStatus);
                    // console.log(data);
                    // console.log(errorThrown);
                    toastr.error('An error occured!')
                },
            });
    
});

//delete stocking
$(".delete-stocking-action").click(function () { 
        var url = $(this).data("href");
        // console.log("url " + url); 
});



// DATE PICKER FILES
   



 $(document).ready(function () {
                
                $('.example1').datepicker({
                    // format: 'yyyy-mm-dd hh:ii'
                    format: "yyyy-mm-dd",
                    showDropdowns: true
                });  
            
            });


</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection