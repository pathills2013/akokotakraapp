@extends('users/layouts.farmsetup-master')
@section('farmsetup')
    <!-- SETUP FARM -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    <ul class="progressbar">
                        <li class="active1">Create Account</li>
                        <li class="active2">Setup Farm</li>
                        <li class="active3">Setup Penhouse</li>
                        <li class="active4">Setup Stock</li>
                        <li class="active5">Completed</li> 
                    </ul>
                </div>
                <div id="smartwizard" class="pt-3">
                    <div class="wz-content-wrapper mt-5 mb-5">    
                        <div class="text-center">
                            <h1>Welcome <b>   
                                {{Auth::user()->name}}

                            </b>, let's setup your farm!</h1>
                            <p>You're well on your way to digitising your poultry farm management and becoming a superhero. First, let's setup your farm.</p>
                            <img class="mt-2" src="{{ URL::to('vendor/poultry-farm.jpg') }}" alt="Poultry Farm" height="100" width="auto">
                        </div>
                        <form id="farm_setup_form" method="POST" action="{{route('onboarding.farmsetup')}}" class="mt-5">
                                {{csrf_field()}} 
                                <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <input autocomplete="off" type="text" class="form-control" 
                                            value="{{$farmaccount->farm_name  ?? ''}}"
                                            name="farmName" id="farmName" aria-describedby="farmName" 
                                            placeholder="Farm Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6">
                                        <div class="form-group">
                                        <input autocomplete="off" type="text" class="form-control" value="{{$farmaccount->farm_address  ?? ''}}" name="farmAddress" id="farmAddress" aria-describedby="farmAddress" placeholder="Farm Address (Street, City)" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <select class="form-control" value="{{$farmaccount->country  ?? ''}}" name="country" id="farmCountry" required>
                                                <option disabled>--Select Country--</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Togo">Togo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6">
                                        <div class="form-group">
                                        <input type="text" autocomplete="off" class="form-control" value="{{$farmaccount->farm_capacity ?? ''}}" name="numberOfBirds" id="numberOfBirds" aria-describedby="numberOfBirds" placeholder="Number of Birds" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6">
                                        <div class="form-group">
                                        <select class="form-control"  name="currency" id="currency" required>
                                                <option disabled>Currency</option>
                                                <option value="GHS">Ghanaian Cedi (GHS)</option>
                                                <option value="NGN">Nigeria Naira (NGN)</option>
                                                <option value="CFA">CFA </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="modal-footer">
                                <button onclick="return confirm('Are you sure you want to proceed?');" id="nextbtn"  class="btn btn-primary" data-dismiss="modal">Next</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection